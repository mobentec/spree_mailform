class MessageMailer < ApplicationMailer
  # use your own email address here
  default :to => "info@soapysoapcompany.com"

  def message_me(msg)
    @msg = msg

    mail from: @msg.email, subject: @msg.subject, body: "Name: "+@msg.name+"<br>"+@msg.content
	
	mail(from: @msg.email, subject: @msg.subject, body: "Name: "+@msg.name+"<br>"+@msg.content) do |format|
      format.html { render 'mailer' }
      format.text { render text: 'mailer' }
    end
  end
end
