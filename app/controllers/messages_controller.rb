class MessagesController < ApplicationController
  def new
    @message = Message.new
  end

  def create
    @message = Message.new(message_params)

    if @message.valid?
      MessageMailer.message_me(@message).deliver_now
	  flash.now[:error] = nil
      flash.now[:notice] = 'Thank you for your message!'
    else
	  flash.now[:error] = 'Cannot send message.'
      render :new
    end
	end

  private
    def message_params
      params.require(:message).permit(:name, :email, :subject, :content)
    end 
end
